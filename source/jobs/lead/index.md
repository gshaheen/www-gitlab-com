---
layout: markdown_page
title: "Lead"
---

Leads carry responsibility for a certain topic.
They keep track of issues in this topic and/or spend the majority of their time there.
Commonly the lead in a topic that makes the final calls.
